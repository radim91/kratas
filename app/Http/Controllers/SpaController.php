<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Link;
use Illuminate\Support\Facades\DB;

class SpaController extends Controller
{
    public function index()
    {
        return view('spa');
    }

    public function fetchLinks(int $number)
    {
        $fetchedLinks = DB::table('links')
            ->where('public', 1)
            ->orderBy('created_at', 'DESC')
            ->limit($number)
            ->get();
        return response()->json($fetchedLinks);
    }

    public function fetch($link)
    {
        $count = DB::table("links")->where('generated_url', $link)->count();
        return response()->json(['count' => $count]);
    }

    public function insert(Request $request)
    {
        if (Link::validator($request)) {
            $msg = ['notification' => 'Odkaz úspěšně zkrácen a uložen do vaší schránky!'];
            $createdLink = ['link' => Link::create([
                "original_url" => $request->original_url,
                "generated_url" => $request->generated_url,
                "public" => $request->public,
            ])];

            return response()->json(array_merge($msg, $createdLink));
        } else {
            $msg = ['notification' => 'Odkaz se nepodařilo uložit.'];

            return response()->json($msg);
        }
    }

    public function show (Link $link) {
        return redirect($link->original_url);
    }
}
