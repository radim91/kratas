<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Link extends Model
{
    protected $guarded = ['id'];

    private const RULES = [
      'original_url' => ['required', 'url'],
      'generated_url' => ['required', 'max:20'],
      'public' => ['nullable', 'integer']
    ];

    public static function validator(Request $request)
    {
        return $request->validate(self::RULES);
    }
}
