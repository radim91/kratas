import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)
 
import History from './views/History'
import Home from './views/Home' 

const router = new VueRouter ({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/historie',
            name: 'history',
            component: History
        }
    ]
})

export default router;
