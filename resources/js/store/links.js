import axios from "axios";

export const namespaced = true;

export const state = {
    count: 0,
    links: [],
    link: null,
    notification: null
};

export const mutations = {
    FETCH_LINKS(state, links) {
        state.links = links;
    },
    FETCH_COUNT(state, count) {
        state.count = count;
    },
    SET_LINK(state, link) {
        state.link = link;
    },
    SET_NOTIFICATION(state, notification) {
        state.notification = notification;
    }
};

export const actions = {
    fetchLinks({ commit }, number) {
        axios
            .get("/api/fetch_links/" + number)
            .then(response => {
                commit("FETCH_LINKS", response.data);
            })
            .catch(error => {
                console.log(error);
            });
    },
    fetchLinkCount({ commit }, link) {
        axios
            .get("/api/fetch/" + link)
            .then(response => {
                commit("FETCH_COUNT", response.data);
            })
            .catch(error => {
                console.log(error);
            });
    },
    insert({ commit }, link) {
        axios.post("/api/insert/", link).then(response => {
            commit("SET_LINK", response.data.link);
            commit("SET_NOTIFICATION", response.data.notification);
        });
    }
};
