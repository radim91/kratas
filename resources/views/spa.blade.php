<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <title>Zkraťsi.net</title>
</head>
<body>
    <div id="app">
        <div class="container">
            <app></app>
        </div>
    </div>


    <footer>
        <div class="container">

            <div class="row justify-content-center mt-5 mt-2">
                <div class="col-sm-12 text-center">
                <span class="copyright">
                &copy; 2020 <a href="https://radimhejduk.cz">radimhejduk.cz</a>
            </span>
                </div>
            </div>
        </div>
    </footer>

    <script src="{{ mix('js/app.js') }}"></script>
</body>


</html>
